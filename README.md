# Christopher Elibert

## Software Engineer/ Data Scientist

With a passion in software development, my ideal career is becoming a data engineer with a concentration in artificial intelligence. Over the multiple years I’ve spent in customer service and technical support, I have obtained superb communication skills to support with providing optimal performance as a developer to assist me in serving as a technical liaison with content teams and creators to convey confidence in contributing to the software development phase. From my educational experience of data analysis, using database schemas for graphical visualizations, I am seeking to apply my knowledge of analytical programming to specialize in improving the world’s infrastructure by building the future of communications through simple, yet robust APIs. I believe this would make me a great fit as a Project Manager, system administrator, or full- stack development.

A brief background about me beginning with my educational experience and obtaining my bachelor’s degree in computer science from Morehouse College. With so many different disciplines in the field I never knew exactly what I wanted to do in computer science, so I decide to explore by attending conferences and applying to different training programs. My first exposure in the industry was the fast-growing field of data science & analytics from Correlation One’s (DS4A) Empowerment training program. This experience has elevated my skillset in software engineering and introduced me to other fields in computer science. After my exposure to the meaning of data science, I decided in my senior year to enrolled in College Pathway’s A.I. program offered at Morehouse College. This allowed me to explore Artificial Intelligence related careers across a variety of fields and help me narrow my interest in fields of study for the discipline of computer science. In addition, with a minor in African American Studies, I have developed a keen awareness of the different socio-economic backgrounds and cultures to further bridge the gap of consumer experience through superb customer service and my education to assist developers to teach others how to get started and build. The fundamentals from the (AI4All) Artificial Intelligence program and how the lack of diversity in the field of A.I. perpetuates bias in algorithms, has intertwined my knowledge in africana studies and computer science. After thorough research I would like to say my career goal is to bridge the gap of algorithmic bias by using my education to assist other developers in building more efficient algorithms for underrepresented communities.

## Hobbies and Intrest

## Experience

Languages: JavaScript and Python
***
Tech Stacks:
***
Clubs:
***
Organizations:
